# Práctica Package Manager

Práctica donde buscamos crear una aplicación con node.js utilizando dependencias.

### Prerequisítos

Tener instaladas las siguientes herramientas:

npm
eslint
node-js
log4js
mochajs

## Ejecución

Utilizamos node para ejecutar nuestro index.js de la siguiente manera

```
node index.js
```

ó

```
npm start
```

Debería mostrarnos una salida similar a:

```
[20XX-XX-XXTXX:XX:XX.XXX] [DEBUG] default - Iniciando aplicación en modo de pruebas.
[20XX-XX-XXTXX:XX:XX.XXX] [INFO] default - La app se ha iniciado correctamente.
[20XX-XX-XXTXX:XX:XX.XXX] [WARN] default - Falta el archivo config de la app.
[20XX-XX-XXTXX:XX:XX.XXX] [ERROR] default - No se pudo acceder al sistema de archivos del equipo.
[20XX-XX-XXTXX:XX:XX.XXX] [FATAL] default - La aplicación no se pudo ejecutar en el S.O.
```
También podemos iniciar nuestra prueba del entorno de pruebas

```
npm test
```

Debería mostrarnos algo como esto:

```
[20XX-XX-XXTXX:XX:XX.XXX] [DEBUG] default - Iniciando aplicación en modo de pruebas.
[20XX-XX-XXTXX:XX:XX.XXX] [INFO] default - La app se ha iniciado correctamente.
[20XX-XX-XXTXX:XX:XX.XXX] [WARN] default - Falta el archivo config de la app.
[20XX-XX-XXTXX:XX:XX.XXX] [ERROR] default - No se pudo acceder al sistema de archivos del equipo.
[20XX-XX-XXTXX:XX:XX.XXX] [FATAL] default - La aplicación no se pudo ejecutar en el S.O.

  Probar la suma de 2 números
    ✔ Cinco más cinco es 10
    ✔ Cinco más cinco no son 55


  2 passing (3ms)
```
## Elaborado con:

* [ESLint](https://eslint.org/)
* [Node-js](https://nodejs.org/es)
* [Mochajs](https://mochajs.org/)


## Autor:

* **Diego Martínez** - [DiegoMTZGlz](https://gitlab.com/DiegoMTZGlz)
