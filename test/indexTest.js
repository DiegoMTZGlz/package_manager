const sumar = require('../index');
const assert = require('assert');


describe("Probar la suma de 2 números", ()=>{
    // Afirmar que 5 + 5 = 10
    it("Cinco más cinco es 10", ()=>{
        assert.equal(10, sumar(5, 5));
    });

    //Afirmamos que 5 + 5 no son 55
    it("Cinco más cinco no son 55", ()=>{
        assert.notEqual(55, sumar(5, 5));
    });
});